//1.Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
//Рекурсія це виклик функції самої себе оба через іншу функцію. задачі із рекурсією часто можна вирішити за допомогою циклів проте не завжди. є певні випадки коли використання рекурсії більш доречніше. 

//Завдання:
let userNum = prompt("Tell me a number", "");

while (userNum === null || userNum === 0 || isNaN(userNum)) {
    userNum = prompt("Tell me a number", userNum);
}
console.log(userNum);

console.log(toFactorialNumber(+userNum));

function toFactorialNumber(a) {
  if (a<0){
    return "we can`t do factotial";
  } else if (a==0) {
    return 1;
  } else{
    return a*toFactorialNumber(a-1);
  }
}
